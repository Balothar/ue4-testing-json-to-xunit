
#include <iostream>
#include <exception>

#include "JsonReader.hpp"
#include "Json2XUnit.hpp"
#include "XMLWriter.hpp"

int main(int argc, char** argv) {

    if(argc != 3) {
        throw std::runtime_error("ERROR: argument count has to be 3!");
    }

    // std::cout << "Hello World!" << std::endl;
    std::cout << "starting conversion..." << std::endl;

    input::JsonReader reader(argv[1]);
    
    processing::Json2XUnit processor(reader);
    processor.process();

    output::XMLWriter writer(argv[2]);
    writer.write(processor.get());

    std::cout << "finished conversion." << std::endl;

}