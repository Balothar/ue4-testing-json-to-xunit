
#include "Json2XUnit.hpp"

#include <exception>
#include <sstream>
#include <iomanip>
#include <regex>

#include "boost/algorithm/string.hpp"

namespace processing {
    
    void Json2XUnit::process() {
        processPrimaryAssembly();
    }

    void Json2XUnit::processPrimaryAssembly() {
        nlohmann::json json = input.get();

        std::string assemblyName = "SpaceGame Tests";

        std::string duration    = std::to_string(json["totalDuration"].get<double>());
        std::string nTotal      = std::to_string(json["tests"].size());
        std::string nPassed     = std::to_string(json["succeeded"].get<size_t>() + json["succeededWithWarnings"].get<size_t>());
        std::string nFailed     = std::to_string(json["failed"].get<size_t>());
        std::string nSkipped    = std::to_string(json["notRun"].get<size_t>());

        // create assembly attributes
        boost::property_tree::ptree assembly;
        assembly.put("<xmlattr>.name", assemblyName);
        assembly.put("<xmlattr>.time", duration);
        assembly.put("<xmlattr>.total", nTotal);
        assembly.put("<xmlattr>.passed", nPassed);
        assembly.put("<xmlattr>.failed", nFailed);
        assembly.put("<xmlattr>.skipped", nSkipped);
        // create empty attributes
        assembly.put("<xmlattr>.config-file", "");
        assembly.put("<xmlattr>.test-framework", "Unreal Engine 4");
        assembly.put("<xmlattr>.environment", "");
        assembly.put("<xmlattr>.errors", "0");

        // collect all tests in their respective collections
        std::unordered_map<std::string, CollectionProperties> collections;
        for(const nlohmann::json& testJson : json["tests"]) {
            TestHolder test = processOneTest(testJson);
            modifyCollection(collections[test.collectionName], test);
        }

        DateTime runStart { Date::max(), Time::max() };
        bool runStartWasChanged = false;
        // add collections (with tests) to the assembly
        for(const std::pair<std::string, CollectionProperties>& collection : collections) {
            std::string name = collection.first;
            CollectionProperties properties = collection.second;
            // create collection node
            boost::property_tree::ptree collectionNode;
            // add attributes
            collectionNode.put("<xmlattr>.name", name);
            collectionNode.put("<xmlattr>.total", properties.nTotal);
            collectionNode.put("<xmlattr>.passed", properties.nPassed);
            collectionNode.put("<xmlattr>.failed", properties.nFailed);
            collectionNode.put("<xmlattr>.skipped", properties.nSkipped);
            collectionNode.put("<xmlattr>.time", std::to_string(properties.timestamps.elapsedTime));
            // add tests
            for(const boost::property_tree::ptree& testNode : properties.tests) {
                collectionNode.add_child("test", testNode);
            }
            // correct run start-date and -time if necessary
            if(properties.timestamps.start < runStart) {
                runStart = properties.timestamps.start;
                runStartWasChanged = true;
            }
            // finally add the finished collection to the assembly
            assembly.add_child("collection", collectionNode);
        }
        // add run start-date and -time to the assembly if they were changed
        if(runStartWasChanged) {
            assembly.put("<xmlattr>.run-date", runStart.date.toStamp());
            assembly.put("<xmlattr>.run-time", runStart.time.toStamp());
        }
        // otherwise leave them empty
        else {
            assembly.put("<xmlattr>.run-date", "");
            assembly.put("<xmlattr>.run-time", "");
        }

        // add the finished assembly to the root-node
        output.add_child("assemblies.assembly", assembly);
    }

    std::pair<std::string, TestResult> Json2XUnit::getResult(const std::string& state) {
        if(state == "Success") {
            return { "Pass", TestResult::Passed };
        }
        else if(state == "Fail") {
            return { "Fail", TestResult::Failed };
        }
        else {
            return { "Skip", TestResult::Skipped };
        }
    }

    TestHolder Json2XUnit::processOneTest(const nlohmann::json& testObject) {
        std::string name = testObject["fullTestPath"].get<std::string>();

        // split the string to extract the collection name
        std::vector<std::string> parts;
        boost::algorithm::split(parts, name, boost::is_any_of("."), boost::algorithm::token_compress_on);

        std::string collectionName = "DefaultCollection";
        std::string testName = name;
        if(parts.size() > 2) {
            collectionName = parts[1];
            testName.clear();
            std::for_each(parts.begin() + 2, parts.end(), [&testName] (const std::string& part) {
                testName += part + ".";
            });
            if(!testName.empty()) {
                testName.erase(testName.end() - 1);
            }
        }
        else if(parts.size() == 2) {
            testName = parts[1];
        }

        // get test timestamps
        Timestamps timestamps = extractTimestamps(testObject);

        // get test results
        std::string result;
        TestResult resultType;
        std::tie(result, resultType) = getResult(testObject["state"].get<std::string>());

        boost::property_tree::ptree node;
        // add attributes
        node.put("<xmlattr>.name", testName);
        node.put("<xmlattr>.result", result);
        node.put("<xmlattr>.time", std::to_string(timestamps.elapsedTime));
        // potentially adjust naming convention to make these more accurate
        std::string type;
        std::for_each(parts.begin(), parts.end() - 1, [&type] (const std::string part) {
            type += part + ".";
        });
        if(!type.empty()) type.erase(type.end() - 1);
        node.put("<xmlattr>.type", type);
        node.put("<xmlattr>.method", parts.back());

        if(resultType == TestResult::Failed) {
            // failure, add error-messages to the test (if they exist)
            std::vector<std::string> messages = extractFailureMessages(testObject);
            if(!messages.empty()) {
                // create and prepare failure node
                boost::property_tree::ptree failure;
                failure.put("<xmlattr>.exception-type", "");
                // construct complete message from complete set of error messages
                std::string completeMessage;
                std::for_each(messages.begin(), messages.end(), [&completeMessage] (const std::string& message) {
                    completeMessage.append(message);
                    completeMessage.append("\n");
                });
                // add complete message as CDATA-node
                failure.put("message", "<![CDATA[\n" + completeMessage + "]]>");
            	// this will need further post processing after writing
                node.add_child("failure", failure);
            }
        }

        return { node, resultType, collectionName, timestamps };
    }

    void Json2XUnit::modifyCollection(CollectionProperties& collection, const TestHolder& test) const {
        // increase total number of tests
        ++collection.nTotal;
        // add test-node to collection
        collection.tests.emplace_back(test.testNode);
        // update timestamps if necessary
        if(test.timestamps.start < collection.timestamps.start) collection.timestamps.start = test.timestamps.start;
        if(test.timestamps.end > collection.timestamps.end) collection.timestamps.end = test.timestamps.end;
        // update elapsed time
        collection.timestamps.elapsedTime = test.timestamps.elapsedTime;
        switch(test.result) {
        case TestResult::Passed:
            ++collection.nPassed;
            break;
        case TestResult::Failed:
            ++collection.nFailed;
            break;
        case TestResult::Skipped:
            ++collection.nSkipped;
            break;
        default:
            throw std::logic_error("ERROR: this should never happend, check the TestResult enum!");
        }
    }

    const std::string Json2XUnit::datePattern("([0-9]{4})-(0[1-9]|1[0-2])-([0][1-9]|[1-2][0-9]|[3][0-1])");
    const std::string Json2XUnit::timePattern("([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])");

    Timestamps Json2XUnit::extractTimestamps(const nlohmann::json& testNode) const {
        Timestamps timestamps = { };
        for(const std::pair<std::string, std::string>& valueString : extractValueStrings(testNode)) {

            std::string key = valueString.first;
            std::string value = valueString.second;
            if(key == "start-date") {
                timestamps.start.date = extractDate(value);
            }
            else if(key == "start-time") {
                timestamps.start.time = extractTime(value);
            }
            else if(key == "end-date") {
                timestamps.end.date = extractDate(value);
            }
            else if(key == "end-time") {
                timestamps.end.time = extractTime(value);
            }
            else if(key == "run-duration") {
                timestamps.elapsedTime = std::stod(value);
            }
        }
        return timestamps;
    }

    std::unordered_map<std::string, std::string> Json2XUnit::extractValueStrings(const nlohmann::json& testNode) const {
        std::unordered_map<std::string, std::string> valueStringMap;
        nlohmann::json::const_iterator entries = testNode.find("entries");
        if(entries != testNode.cend()) {
            // "entries" exists, iterate
            for(const nlohmann::json& entry : *entries) {
                std::string type = entry["event"]["type"].get<std::string>();
                if(type == "Info") {
                    std::string message = entry["event"]["message"].get<std::string>();
                    if(boost::starts_with(message, "!!!TEST TIMING EVENT!!!")) {
                        std::regex pattern("\\[\\[ (.+?)=((" + datePattern + ")|(" + timePattern + ")|([0-9]+(\\.[0-9]*)?)) \\]\\]");
                        std::smatch results;
                        if(std::regex_search(message, results, pattern)) {
                            // match was found, add to map
                            valueStringMap[results[1].str()] = results[2].str();
                        }
                    }
                }
            }
        }
        return valueStringMap;
    }

    Date Json2XUnit::extractDate(const std::string& value) const {
        std::regex pattern(datePattern);
        std::smatch results;
        if(std::regex_match(value, results, pattern)) {
            return { static_cast<uint32_t>(std::stoi(results[1].str())), static_cast<uint8_t>(std::stoi(results[2].str())), 
                static_cast<uint8_t>(std::stoi(results[3].str())) };
        }
        // invalid, return "default" date
        return { 0, 0, 0 };
    }

    Time Json2XUnit::extractTime(const std::string& value) const {
        std::regex pattern(timePattern);
        std::smatch results;
        if(std::regex_match(value, results, pattern)) {
            return { static_cast<uint8_t>(std::stoi(results[1].str())), static_cast<uint8_t>(std::stoi(results[2].str())), 
                static_cast<uint8_t>(std::stoi(results[3].str())) };
        }
        // invalid, return "default" date
        return { 0, 0, 0 };
    }

    std::string Date::toStamp() const {
        std::stringstream stamp;
        stamp << static_cast<int64_t>(year) << "-" 
            << std::setw(2) << std::setfill('0') << static_cast<int64_t>(month) << "-" 
            << std::setw(2) << std::setfill('0') << static_cast<int64_t>(day);
        return stamp.str();
    }

    std::string Time::toStamp() const {
        std::stringstream stamp;
        stamp << std::setw(2) << std::setfill('0') << static_cast<int64_t>(hour) << ":" 
            << std::setw(2) << std::setfill('0') << static_cast<int64_t>(minute) << ":"
            << std::setw(2) << std::setfill('0') << static_cast<int64_t>(second);
        return stamp.str();
    }

    const std::vector<std::string> Json2XUnit::extractFailureMessages(const nlohmann::json& testNode) const {
        std::vector<std::string> messages;
        nlohmann::json::const_iterator entries = testNode.find("entries");
        if(entries != testNode.cend()) {
            for(const nlohmann::json& entry : *entries) {
                std::string type = entry["event"]["type"].get<std::string>();
                if(type == "Error") {
                    messages.emplace_back(entry["event"]["message"].get<std::string>());
                }
            }
        }
        return messages;
    }
}