#pragma once

#include <unordered_map>
#include <string>
#include <utility>
#include <vector>
#include <cstdint>
#include <regex>
#include <limits>

#include "boost/property_tree/ptree.hpp"

#include "JsonReader.hpp"

namespace processing {

    enum class TestResult {
        Passed,
        Failed,
        Skipped
    };

    struct Date {
        uint32_t year;
        uint8_t month, day;
        std::string toStamp() const;
        static const Date max() {
            return { std::numeric_limits<uint32_t>::max(), 12, 31 };
        }
        static const Date min() {
            return { };
        }
    };


    inline const bool operator == (const Date& lhs, const Date& rhs) {
        return lhs.year == rhs.year && lhs.month == rhs.month && lhs.day == rhs.day;
    }
    inline const bool operator > (const Date& lhs, const Date& rhs) {
        return
            (lhs.year  > rhs.year)                                                  ||
            (lhs.year == rhs.year && lhs.month  > rhs.month)                        || 
            (lhs.year == rhs.year && lhs.month == rhs.month && lhs.day > lhs.day);
    }
    inline const bool operator < (const Date& lhs, const Date& rhs) {
        return 
            (lhs.year  < rhs.year)                                                  ||
            (lhs.year == rhs.year && lhs.month  < rhs.month)                        || 
            (lhs.year == rhs.year && lhs.month == rhs.month && lhs.day < lhs.day);
    }
    inline const bool operator >= (const Date& lhs, const Date& rhs) {
        return lhs == rhs || lhs > rhs;
    }
    inline const bool operator <= (const Date& lhs, const Date& rhs) {
        return lhs == rhs || lhs < rhs;
    }

    struct Time {
        uint8_t hour, minute, second;
        std::string toStamp() const;
        static const Time max() {
            return { 23, 59, 59 };
        }        
        static const Time min() {
            return { };
        }
    };

    inline const bool operator == (const Time& lhs, const Time& rhs) {
        return lhs.hour == rhs.hour && lhs.minute == rhs.minute && lhs.second == rhs.minute;
    }
    inline const bool operator > (const Time& lhs, const Time& rhs) {
        return 
            (lhs.hour  < rhs.hour)                                                          ||
            (lhs.hour == rhs.hour && lhs.minute  < rhs.minute)                              || 
            (lhs.hour == rhs.hour && lhs.minute == rhs.minute && lhs.second < lhs.second);
    }
    inline const bool operator < (const Time& lhs, const Time& rhs) {
        return
            (lhs.hour  > rhs.hour)                                                          ||
            (lhs.hour == rhs.hour && lhs.minute  > rhs.minute)                              || 
            (lhs.hour == rhs.hour && lhs.minute == rhs.minute && lhs.second > lhs.second);
    }
    inline const bool operator >= (const Time& lhs, const Time& rhs) {
        return lhs > rhs || lhs == rhs;
    }
    inline const bool operator <= (const Time& lhs, const Time& rhs) {
        return lhs < rhs || lhs == rhs;
    }

    struct DateTime {
        Date date;
        Time time;
    };

    inline bool operator == (const DateTime& lhs, const DateTime& rhs) {
        return lhs.date == rhs.date && lhs.time == rhs.time;
    }
    inline bool operator > (const DateTime& lhs, const DateTime& rhs) {
        return lhs.date > rhs.date || (lhs.date == rhs.date && lhs.time > rhs.time);
    }
    inline bool operator < (const DateTime& lhs, const DateTime& rhs) {
        return lhs.date < rhs.date || (lhs.date == rhs.date && lhs.time < rhs.time);
    }
    inline bool operator >= (const DateTime& lhs, const DateTime& rhs) {
        return lhs == rhs || lhs > rhs;
    }
    inline bool operator <= (const DateTime& lhs, const DateTime& rhs) {
        return lhs == rhs || lhs < rhs;
    }

    struct Timestamps {
        double elapsedTime;
        DateTime start;
        DateTime end;
    };

    struct TestHolder {
        boost::property_tree::ptree testNode;
        TestResult result;
        std::string collectionName;
        Timestamps timestamps;
    };

    struct CollectionProperties {
        std::vector<boost::property_tree::ptree> tests;
        size_t nTotal;
        size_t nPassed;
        size_t nFailed;
        size_t nSkipped;
        Timestamps timestamps;
        CollectionProperties() : tests(), nTotal(), nPassed(), nFailed(), nSkipped(), 
            timestamps({0, DateTime({ Date::max(), Time::max() }), DateTime({ Date::min(), Time::min() })}) { }
    };

    class Json2XUnit {
        input::JsonReader input;
        boost::property_tree::ptree output;

        static const std::string datePattern;
        static const std::string timePattern;

    public:
        Json2XUnit(const input::JsonReader& in) : input(in), output() { }
        void process();
        boost::property_tree::ptree get() { return output; }
    private:
        void processPrimaryAssembly();
        TestHolder processOneTest(const nlohmann::json& testObject);
        void modifyCollection(CollectionProperties& collection, const TestHolder& test) const;

        Timestamps extractTimestamps(const nlohmann::json& testNode) const;
        std::unordered_map<std::string, std::string> extractValueStrings(const nlohmann::json& testNode) const;

        Date extractDate(const std::string& value) const;
        Time extractTime(const std::string& value) const;

        const std::vector<std::string> extractFailureMessages(const nlohmann::json& testNode) const;

        std::pair<std::string, TestResult> getResult(const std::string& state);
    };
}