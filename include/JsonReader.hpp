#pragma once

#include <string>
#include <fstream>
#include <filesystem>

#include "json.hpp"

namespace input {
    class JsonReader {
        std::filesystem::path testReportFile;
        nlohmann::json report;
    public:
        JsonReader(const std::string& filename) : testReportFile(filename), report() {
            // parse ue4 test report
            std::ifstream reportFile(testReportFile.string());
            reportFile >> report;
        }

        nlohmann::json get() const { return report; }
    };
}