#pragma once

#include <string>
#include <fstream>
#include <filesystem>

#include "boost/property_tree/ptree.hpp"
#include "boost/property_tree/xml_parser.hpp"
#include "boost/algorithm/string/replace.hpp"

namespace output {
    class XMLWriter {
        std::filesystem::path outXMLFile;
    public:
        XMLWriter(const std::string& path) : outXMLFile(path) { }
        void write(const boost::property_tree::ptree& xml) {
            std::ofstream out;
            out.open(outXMLFile.string());
            if(out.is_open()) {
                // write xml data to a stream
                std::stringstream temporaryXml;
                boost::property_tree::write_xml(temporaryXml, xml, boost::property_tree::xml_writer_make_settings<std::string>(' ', 4));

            	// perform post-processing to fix CDATA nodes
            	// (see https://stackoverflow.com/questions/54038989/write-cdata-xml-node-with-boostproperty-tree/60735225#60735225)
                auto cleanedXML = boost::replace_all_copy(temporaryXml.str(), "&gt;", ">");
                cleanedXML = boost::replace_all_copy(cleanedXML, "&lt;", "<");

            	// write file
                out << cleanedXML;

            	// close file
                out.close();
            }       	
        }
    };
}